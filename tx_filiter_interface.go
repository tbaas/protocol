/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package protocol

import "chainmaker.org/chainmaker/pb-go/v2/common"

type TxFilter interface {
	GetHeight() uint64
	SetHeight(height uint64)
	Add(txId string) error
	// Adds add transactions to the filter in batches, and log and return an array of abnormal transactions if an exception occurs
	Adds(txIds []string) error
	// IsExists ruleType see chainmaker.org/chainmaker/protocol/v2/birdsnest.RulesType
	IsExists(txId string, ruleType ...common.RuleType) (bool, error)
	// ValidateRule validate rules
	ValidateRule(txId string, ruleType ...common.RuleType) error
	IsExistsAndReturnHeight(txId string, ruleType ...common.RuleType) (bool, uint64, error)
	AddsAndSetHeight(txId []string, height uint64) (result error)
	Close()
}
