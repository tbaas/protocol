/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

// define vm parameter and interface
package protocol

import (
	"bytes"
	"fmt"
	"regexp"

	pbac "chainmaker.org/chainmaker/pb-go/v2/accesscontrol"

	"chainmaker.org/chainmaker/pb-go/v2/common"
)

const (
	GasLimit            = 1e10    // invoke user contract max gas
	TimeLimit           = 1 * 1e9 // 1s
	CallContractGasOnce = 1e5     // Gas consumed per cross call contract
	CallContractDepth   = 5       // cross call contract stack depth, must less than vm pool min size
	EvmGasPrice         = 1
	EvmMaxStackDepth    = 1024

	ContractSdkSignalResultSuccess = 0 // sdk call chain method success result
	ContractSdkSignalResultFail    = 1 // sdk call chain method success result

	DefaultMaxStateKeyLen = 1024                // key & name for contract state length
	DefaultStateRegex     = "^[a-zA-Z0-9._-]+$" // key & name for contract state regex

	DefaultVersionLen   = 64                  // key & name for contract state length
	DefaultVersionRegex = "^[a-zA-Z0-9._-]+$" // key & name for contract state regex

	ParametersKeyMaxCount           = 50 //
	DefaultParametersValueMaxSize = 10 // default size transaction's per parameter (MB)

	TopicMaxLen       = 255
	EventDataMaxLen   = 65535
	EventDataMaxCount = 16

	ContractStoreSeparator = "#"

	// special parameters passed to contract
	ContractCreatorOrgIdParam = "__creator_org_id__"
	ContractCreatorRoleParam  = "__creator_role__"
	ContractCreatorPkParam    = "__creator_pk__"
	ContractSenderOrgIdParam  = "__sender_org_id__"
	ContractSenderRoleParam   = "__sender_role__"
	ContractSenderPkParam     = "__sender_pk__"
	ContractBlockHeightParam  = "__block_height__"
	ContractTxIdParam         = "__tx_id__"
	ContractTxTimeStamp       = "__tx_time_stamp__"
	ContractContextPtrParam   = "__context_ptr__"
	ContractAddrTypeParam     = "__addr_type__"
	ContractSenderTypeParam   = "__sender_type__"
	ContractCreatorTypeParam  = "__creator_type__"

	// user contract must implement such method
	ContractInitMethod        = "init_contract"
	ContractUpgradeMethod     = "upgrade"
	ContractAllocateMethod    = "allocate"
	ContractDeallocateMethod  = "deallocate"
	ContractRuntimeTypeMethod = "runtime_type"
	ContractEvmParamKey       = "data"
	// method name used by smart contract sdk
	// common
	ContractMethodLogMessage      = "LogMessage"
	ContractMethodSuccessResult   = "SuccessResult"
	ContractMethodErrorResult     = "ErrorResult"
	ContractMethodCallContract    = "CallContract"
	ContractMethodCallContractLen = "CallContractLen"
	// kv
	ContractMethodGetStateLen = "GetStateLen"
	ContractMethodGetState    = "GetState"
	ContractMethodPutState    = "PutState"
	ContractMethodDeleteState = "DeleteState"
	// kv iterator author:whang1234
	ContractMethodKvIterator        = "KvIterator"
	ContractMethodKvPreIterator     = "KvPreIterator"
	ContractMethodKvIteratorHasNext = "KvIteratorHasNext"
	ContractMethodKvIteratorNextLen = "KvIteratorNextLen"
	ContractMethodKvIteratorNext    = "KvIteratorNext"
	ContractMethodKvIteratorClose   = "KvIteratorClose"
	// sql
	ContractMethodExecuteQuery       = "ExecuteQuery"
	ContractMethodExecuteQueryOne    = "ExecuteQueryOne"
	ContractMethodExecuteQueryOneLen = "ExecuteQueryOneLen"
	ContractMethodRSNext             = "RSNext"
	ContractMethodRSNextLen          = "RSNextLen"
	ContractMethodRSHasNext          = "RSHasNext"
	ContractMethodRSClose            = "RSClose"
	ContractMethodExecuteUpdate      = "ExecuteUpdate"
	ContractMethodExecuteDdl         = "ExecuteDDL"
	ContractMethodEmitEvent          = "EmitEvent"
	// paillier
	ContractMethodGetPaillierOperationResult    = "GetPaillierOperationResult"
	ContractMethodGetPaillierOperationResultLen = "GetPaillierOperationResultLen"
	PaillierOpTypeAddCiphertext                 = "AddCiphertext"
	PaillierOpTypeAddPlaintext                  = "AddPlaintext"
	PaillierOpTypeSubCiphertext                 = "SubCiphertext"
	PaillierOpTypeSubPlaintext                  = "SubPlaintext"
	PaillierOpTypeNumMul                        = "NumMul"
	// bulletproofs
	ContractMethodGetBulletproofsResult     = "GetBulletproofsResult"
	ContractMethodGetBulletproofsResultLen  = "GetBulletproofsResultLen"
	BulletProofsOpTypePedersenAddNum        = "PedersenAddNum"
	BulletProofsOpTypePedersenAddCommitment = "PedersenAddCommitment"
	BulletProofsOpTypePedersenSubNum        = "PedersenSubNum"
	BulletProofsOpTypePedersenSubCommitment = "PedersenSubCommitment"
	BulletProofsOpTypePedersenMulNum        = "PedersenMulNum"
	BulletProofsVerify                      = "BulletproofsVerify"
)

var (
	ParametersValueMaxLength uint32
)

type ExecOrderTxType int

const (
	ExecOrderTxTypeNormal ExecOrderTxType = iota
	ExecOrderTxTypeIterator
	ExecOrderTxTypeChargeGas
)

type SqlType int8

const (
	SqlTypeDdl SqlType = iota
	SqlTypeDml
	SqlTypeDql
)

//VmManager manage vm runtime
type VmManager interface {
	// GetAccessControl get accessControl manages policies and principles
	GetAccessControl() AccessControlProvider
	// GetChainNodesInfoProvider get ChainNodesInfoProvider provide base node info list of chain.
	GetChainNodesInfoProvider() ChainNodesInfoProvider
	// RunContract run native or user contract according ContractName in contractId, and call the specified function
	RunContract(contract *common.Contract, method string, byteCode []byte, parameters map[string][]byte,
		txContext TxSimContext, gasUsed uint64, refTxType common.TxType) (
		*common.ContractResult, ExecOrderTxType, common.TxStatusCode)
	// Start all vm instance
	Start() error
	// Stop all vm instance
	Stop() error
}

//RuntimeInstance of smart contract engine runtime
type RuntimeInstance interface {
	// start vm runtime with invoke, call “method”
	Invoke(contractId *common.Contract, method string, byteCode []byte, parameters map[string][]byte,
		txContext TxSimContext, gasUsed uint64) (*common.ContractResult, ExecOrderTxType)
}

type VmInstancesManager interface {
	NewRuntimeInstance(txSimContext TxSimContext, chainId, method, codePath string, contract *common.Contract,
		byteCode []byte, log Logger) (RuntimeInstance, error)
	// StartVM Start vm
	StartVM() error
	// StopVM Stop vm
	StopVM() error
}

type ContractWacsiCommon interface {
	LogMessage() int32
	SuccessResult() int32
	ErrorResult() int32
	CallContract() int32
}

type ContractWacsiKV interface {
	ContractWacsiCommon
	GetState() int32
	PutState() int32
	DeleteState() int32
	KvIterator() int32
	KvPreIterator() int32
	KvIteratorClose() int32
	KvIteratorNext() int32
	KvIteratorHasNext() int32
}

type ContractWacsiSQL interface {
	ContractWacsiCommon
	ExecuteQuery() int32
	ExecuteQueryOne() int32
	RSHasNext() int32
	RSNext() int32
	RSClose() int32
	ExecuteUpdate() int32
	ExecuteDDL() int32
}

// GetKeyStr get state key from string
func GetKeyStr(key string, field string) []byte {
	return GetKey([]byte(key), []byte(field))
}

// GetKey get state key from byte
func GetKey(key []byte, field []byte) []byte {
	var buf bytes.Buffer
	buf.Write(key)
	if len(field) > 0 {
		buf.Write([]byte(ContractStoreSeparator))
		buf.Write(field)
	}
	return buf.Bytes()
}

// CheckKeyFieldStr verify param
func CheckKeyFieldStr(key string, field string) error {
	{
		s := key
		if len(s) > DefaultMaxStateKeyLen {
			return fmt.Errorf("key[%s] too long", s)
		}
		match, err := regexp.MatchString(DefaultStateRegex, s)
		if err != nil || !match {
			return fmt.Errorf("key[%s] can only consist of numbers, dot, letters and underscores", s)
		}
	}
	{
		s := field
		if len(s) == 0 {
			return nil
		}
		if len(s) > DefaultMaxStateKeyLen {
			return fmt.Errorf("key field[%s] too long", s)
		}
		match, err := regexp.MatchString(DefaultStateRegex, s)
		if err != nil || !match {
			return fmt.Errorf("key field[%s] can only consist of numbers, dot, letters and underscores", s)
		}
	}
	return nil
}

//CheckTopicStr
func CheckTopicStr(topic string) error {
	topicLen := len(topic)
	if topicLen == 0 {
		return fmt.Errorf("topic can not empty")
	}
	if topicLen > TopicMaxLen {
		return fmt.Errorf("topic too long,longer than %v", TopicMaxLen)
	}
	return nil

}

//CheckEventTopicTableData  verify event data
func CheckEventData(eventData []string) error {

	eventDataNum := len(eventData)
	if eventDataNum == 0 {
		return fmt.Errorf("event data can not empty")

	}
	if eventDataNum > EventDataMaxCount {
		return fmt.Errorf("too many event data")
	}
	for _, data := range eventData {
		if len(data) > EventDataMaxLen {
			return fmt.Errorf("event data too long,longer than %v", EventDataMaxLen)
		}
	}
	return nil

}

// The simulated execution context of the transaction, providing a cache for the transaction to read and write
type TxSimContext interface {
	// Get key from cache, record this operation to read set
	Get(contractName string, key []byte) ([]byte, error)
	//GetNoRecord read data from state, but not record into read set, only used for framework
	GetNoRecord(contractName string, key []byte) ([]byte, error)
	// Put key into cache
	Put(name string, key []byte, value []byte) error
	// PutRecord put sql state into cache
	PutRecord(contractName string, value []byte, sqlType SqlType)
	// PutIntoReadSet put kv to readset
	PutIntoReadSet(contractName string, key []byte, value []byte)
	// Del Delete key from cache
	Del(name string, key []byte) error
	// Select range query for key [start, limit)
	Select(name string, startKey []byte, limit []byte) (StateIterator, error)
	// GetHistoryIterForKey query the change history of a key in a contract
	GetHistoryIterForKey(contractName string, key []byte) (KeyHistoryIterator, error)
	// CallContract Cross contract call, return (contract result, gas used)
	CallContract(contract *common.Contract, method string, byteCode []byte,
		parameter map[string][]byte, gasUsed uint64, refTxType common.TxType) (
		*common.ContractResult, ExecOrderTxType, common.TxStatusCode)
	// GetCurrentResult Get cross contract call result, cache for len
	GetCurrentResult() []byte
	// GetTx get related transaction
	GetTx() *common.Transaction
	// GetBlockHeight returns current block height
	GetBlockHeight() uint64
	// GetBlockTimestamp returns current block timestamp
	GetBlockTimestamp() int64
	// Get current block proposer
	GetBlockProposer() *pbac.Member
	// Get the tx result
	GetTxResult() *common.Result
	// Set the tx result
	SetTxResult(*common.Result)
	// Get the read and write set completed by the current transaction
	GetTxRWSet(runVmSuccess bool) *common.TxRWSet
	// Get the creator of the contract
	GetCreator(namespace string) *pbac.Member
	// Get the invoker of the transaction
	GetSender() *pbac.Member
	// Get related blockchain store
	GetBlockchainStore() BlockchainStore
	// Get access control service
	GetAccessControl() (AccessControlProvider, error)
	// Get organization service
	GetChainNodesInfoProvider() (ChainNodesInfoProvider, error)
	// The execution sequence of the transaction, used to construct the dag,
	// indicating the number of completed transactions during transaction scheduling
	GetTxExecSeq() int
	SetTxExecSeq(int)
	// Get cross contract call deep
	GetDepth() int
	SetIterHandle(index int32, iter interface{})
	GetIterHandle(index int32) (interface{}, bool)
	GetBlockVersion() uint32
	//GetContractByName get contract info by name
	GetContractByName(name string) (*common.Contract, error)
	//GetContractBytecode get contract bytecode
	GetContractBytecode(name string) ([]byte, error)
}
