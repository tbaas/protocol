/*
 * Copyright (C) BABEC. All rights reserved.
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package test

import (
	"fmt"
	"os"
)

//HoleLogger do record nothing
//不做任何日志记录的Logger，主要用于BenchmarkTest性能测试的场景
type HoleLogger struct {
}

func (h HoleLogger) Debug(args ...interface{}) {

}

func (h HoleLogger) Debugf(format string, args ...interface{}) {

}

func (h HoleLogger) Debugw(msg string, keysAndValues ...interface{}) {

}

func (h HoleLogger) Error(args ...interface{}) {

}

func (h HoleLogger) Errorf(format string, args ...interface{}) {

}

func (h HoleLogger) Errorw(msg string, keysAndValues ...interface{}) {

}

func (h HoleLogger) Fatal(args ...interface{}) {
	os.Exit(1)
}

func (h HoleLogger) Fatalf(format string, args ...interface{}) {
	os.Exit(1)
}

func (h HoleLogger) Fatalw(msg string, keysAndValues ...interface{}) {
	os.Exit(1)
}

func (h HoleLogger) Info(args ...interface{}) {

}

func (h HoleLogger) Infof(format string, args ...interface{}) {

}

func (h HoleLogger) Infow(msg string, keysAndValues ...interface{}) {

}

func (h HoleLogger) Panic(args ...interface{}) {
	panic(args)
}

func (h HoleLogger) Panicf(format string, args ...interface{}) {
	panic(fmt.Sprintf(format, args...))
}

func (h HoleLogger) Panicw(msg string, keysAndValues ...interface{}) {

	panic(fmt.Sprintf(msg+" %v", keysAndValues...))
}

func (h HoleLogger) Warn(args ...interface{}) {

}

func (h HoleLogger) Warnf(format string, args ...interface{}) {

}

func (h HoleLogger) Warnw(msg string, keysAndValues ...interface{}) {

}

func (h HoleLogger) DebugDynamic(getStr func() string) {

}

func (h HoleLogger) InfoDynamic(getStr func() string) {

}
